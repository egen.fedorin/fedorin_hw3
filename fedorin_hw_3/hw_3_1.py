# 1
def get_range(input_stud_dict):
    min_value = 0
    consistent = True

    sorted_students = dict(sorted(input_stud_dict.items(), key=lambda item: item[1]["Score"]))

    for student, details in sorted_students.items():
        max_value = details["Score"]
        if details["Score"] >= min_value and details["Status"] == "Failed":
            min_value = details["Score"]

    for student, details in sorted_students.items():
        if details["Score"] < min_value and details["Status"] == "Passed":
            consistent = False

    return {"min": min_value + 1, "max": max_value, "consistent": consistent}

def print_range(range_dict):
    if range_dict['consistent']:
        print(f"Професор послідовний, поріг складання іспиту в діапазоні {range_dict['min']} - {range_dict['max']} балів")
    else:
        print(f"Професор непослідовний")


students_dict_1 = {
    "Student 1": {"Score": 78, "Status": "Failed"},
    "Student 2": {"Score": 82, "Status": "Passed"},
    "Student 3": {"Score": 97, "Status": "Passed"},
    "Student 4": {"Score": 86, "Status": "Passed"},
    "Student 5": {"Score": 67, "Status": "Passed"},
    "Student 6": {"Score": 75, "Status": "Passed"}
}

students_dict_2 = {
    "Student 1": {"Score": 84, "Status": "Passed"},
    "Student 2": {"Score": 78, "Status": "Passed"},
    "Student 3": {"Score": 65, "Status": "Failed"},
    "Student 4": {"Score": 90, "Status": "Passed"},
    "Student 5": {"Score": 72, "Status": "Failed"}
}

students_dict_3 = {
    "Student 1": {"Score": 50, "Status": "Failed"},
    "Student 2": {"Score": 60, "Status": "Passed"},
    "Student 3": {"Score": 70, "Status": "Failed"},
    "Student 4": {"Score": 80, "Status": "Passed"},
    "Student 5": {"Score": 65, "Status": "Passed"}
}

print_range(get_range(students_dict_1))
print("===========================")

print_range(get_range(students_dict_2))
print("===========================")

print_range(get_range(students_dict_3))
print("===========================")
